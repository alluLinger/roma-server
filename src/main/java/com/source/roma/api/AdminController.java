package com.source.roma.api;

import com.source.roma.domain.User;
import com.source.roma.exception.NotFoundException;
import com.source.roma.service.UserService;
import com.source.roma.util.Message;
import com.source.roma.util.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin
public class AdminController {
    @GetMapping(value = "/logout")
    public Message logoutAdmin(HttpServletRequest request){
        Token t = Token.getToken();
        t.removeAdminToken(request.getHeader("Authorization"));
        return new Message(Message.TYPE.OK, "ok", "");
    }
}
