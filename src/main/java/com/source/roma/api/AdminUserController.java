package com.source.roma.api;

import com.source.roma.domain.User;
import com.source.roma.exception.NotFoundException;
import com.source.roma.service.UserService;
import com.source.roma.util.Message;
import com.source.roma.util.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/user")
@CrossOrigin
public class AdminUserController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "/all")
    public List<User> getUserList()
    {
        return userService.getUserList();
    }

    @PostMapping(value = "/")
    public Object addUser(@RequestBody User user){
        return userService.addUser(user);
    }

    @GetMapping(value = "/{id}")
    public Object getUser(@PathVariable("id") Integer id) throws NotFoundException
    {
        return userService.getUser(id);
    }

    @DeleteMapping(value = "/{id}")
    public Object deleteUser(@PathVariable("id") Integer id)
    {
        if(id == 10000)
            return new Message(Message.TYPE.OK, "你不能删除自己");

        userService.deleteUser(id);
        return new Message(Message.TYPE.OK);
    }

    @PatchMapping(value = "/{id}")
    public User updateUser(@PathVariable("id") Integer id, @RequestBody User user)
    {
        return userService.update(id, user);
    }

}
