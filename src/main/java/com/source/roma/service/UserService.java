package com.source.roma.service;

import com.source.roma.domain.User;
import com.source.roma.exception.NotFoundException;
import com.source.roma.exception.Result;
import com.source.roma.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import static com.source.roma.util.Util.getNullPropertyNames;

@Service
public class UserService {
    @Resource
    private UserRepository userRepository;

    public Object addUser(User user){
        return userRepository.save(user);
    }

    public List<User> getUserList(){
        return userRepository.findAll();
    }

    public Object getUser(Integer id) throws NotFoundException {
        User currentInstance = userRepository.findById(id).get();
        if(currentInstance == null)
            throw new NotFoundException("user" + id + "is not found", Result.ErrorCode.USER_NOT_FOUND.getCode());
        return userRepository.findById(id);
    }

    public Object getUser(String username) {
        return userRepository.findByUsername(username);
    }

    public void deleteUser(Integer id)
    {
        userRepository.deleteById(id);
    }

    public User update(Integer id, User user)
    {
        User currentInstance = userRepository.findById(id).get();

        //支持部分更新
        String[] nullPropertyNames = getNullPropertyNames(user);
        BeanUtils.copyProperties(user, currentInstance, nullPropertyNames);

        return userRepository.save(currentInstance);
    }


}
