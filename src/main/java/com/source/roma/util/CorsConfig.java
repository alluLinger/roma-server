package com.source.roma.util;


import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class CorsConfig implements WebMvcConfigurer  {
    @Override
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT", "OPTIONS")
                .maxAge(3600);
    }

    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new AuthorHandlerInterceptorAdapter());
        WebMvcConfigurer.super.addInterceptors(registry);
    }
}
