package com.source.roma.util;

import com.source.roma.domain.User;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;

class TokenBody {
    private String token;
    private boolean isAdmin; //admin
    private long expire;

    public TokenBody(String token, boolean isAdmin, long expire) {
        this.token = token;
        this.isAdmin = isAdmin;
        this.expire = expire;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

}
public class Token {
    private static MultiValueMap<Integer, String> userToken = new LinkedMultiValueMap<>();
    private static HashMap<String, Integer> tokenUser = new HashMap<>();
    private static HashMap<String, Integer> tokenAdmin = new HashMap<>();
    private static Token singleToken = null;

    private Token(){}
    public static Token getToken() {
        if (singleToken == null) {
            singleToken = new Token();
        }
        return singleToken;
    }

    public TokenBody addUser(User user) {
        System.out.println(user);
        if(userToken.get(user) != null && userToken.get(user).size() > 5) {
            userToken.get(user).remove(0); // first one remove
            tokenUser.remove(userToken.get(user).get(0));
        }
        String uuid = UUID.randomUUID().toString();
        if(user.getId() == 10000){
            tokenAdmin.put(uuid,10000);
            return new TokenBody(uuid, true, (System.currentTimeMillis() +   604800000L ));
        }
        userToken.add(user.getId(), uuid);
        tokenUser.put(uuid, user.getId());
        System.out.println("Hello" + toString());
        return new TokenBody(uuid, false, (System.currentTimeMillis() +  2592000000L ));
    }

    public boolean removeToken(String uuid){
        if(uuid == null || uuid.isEmpty()){
            return true;
        }
        Integer id = tokenUser.get(uuid);
        if(id == null){
            return true;
        }
        userToken.get(id).remove(uuid);
        tokenUser.remove(uuid);
        return true;
    }

    public boolean removeAdminToken(String uuid) {
        if(uuid == null || uuid.isEmpty()){
            return true;
        }
        Integer id = tokenAdmin.get(uuid);
        if(id == null){
            return true;
        }

        tokenAdmin.remove(uuid);
        return true;
    }

    public boolean authToken(String uuid){
        if(tokenUser.get(uuid) == null){
            return false;
        }
        return true;
    }

    public boolean authAdminToken(String uuid){
        if(tokenAdmin.get(uuid) == null){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        for (Map.Entry<Integer, List<String>> e:
             userToken.entrySet()) {
            str.append("User: " + e.getKey());
            for (String k: e.getValue()) {
                str.append("  Key: " + k + "\n");
            }
            System.out.println("---------");
        }

        System.out.println("---tokenUser---");
        for (Map.Entry<String, Integer> e: tokenUser.entrySet()){
            str.append("Key: " + e.getKey());
            str.append(" Value: " + e.getValue());
        }
        return str.toString();
    }


}