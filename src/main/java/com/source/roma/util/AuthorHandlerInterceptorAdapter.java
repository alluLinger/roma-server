package com.source.roma.util;

import com.source.roma.util.Message;
import com.source.roma.util.Token;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AuthorHandlerInterceptorAdapter implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
        System.out.println(request.getRequestURI());
        if(request.getMethod().toLowerCase().equals("options")){
            System.out.println("ignore options");
            return true;
        }
//        System.out.println();
//|| request.getRequestURI().endsWith("logout")
        if(request.getRequestURI().endsWith("login") )
            return true;

        String auth = request.getHeader("Authorization");
        if(auth == null || auth.isEmpty()){
            System.out.println("I'm empty auth");
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.sendError(Message.TYPE.AUTH_FAILED.value());
            return false;
        }else {
            Token t = Token.getToken();
            System.out.println("tokens = " + t);
            System.out.println("auth = " + auth);
            if(request.getRequestURI().contains("admin") ) {
                System.out.println("I'm adminToken");
                if(t.authAdminToken(auth))
                    return true;
            } else {
                System.out.println("I'm uesrToken");
                if(t.authToken(auth))
                    return true;
            };
            System.out.println("tokens = " + t);
        }
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.sendError(Message.TYPE.AUTH_FAILED.value());
        return false;
    }
}